package demin0u.com.rexit_test_app.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DemiN0u on 26.07.2016.
 */
public class UserPhoto implements Parcelable {

    @SerializedName("id")
    private String id;

    @SerializedName("created_at")
    private String created;

    @SerializedName("updated_at")
    private String updated;

    @SerializedName("filename")
    private String filename;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("url")
    private String url;

    protected UserPhoto(Parcel in) {
        id = in.readString();
        created = in.readString();
        updated = in.readString();
        filename = in.readString();
        userId = in.readString();
        url = in.readString();
    }

    public static final Creator<UserPhoto> CREATOR = new Creator<UserPhoto>() {
        @Override
        public UserPhoto createFromParcel(Parcel in) {
            return new UserPhoto(in);
        }

        @Override
        public UserPhoto[] newArray(int size) {
            return new UserPhoto[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(created);
        parcel.writeString(updated);
        parcel.writeString(filename);
        parcel.writeString(userId);
        parcel.writeString(url);
    }
}
