package demin0u.com.rexit_test_app;

import android.app.Application;

import com.facebook.FacebookSdk;

/**
 * Created by DemiN0u on 26.07.2016.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
    }
 }
