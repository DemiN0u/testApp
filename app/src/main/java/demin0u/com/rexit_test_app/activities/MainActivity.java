package demin0u.com.rexit_test_app.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import demin0u.com.rexit_test_app.api.BaseResponse;
import demin0u.com.rexit_test_app.data.UserPhoto;
import demin0u.com.rexit_test_app.fragments.FriendsListFragment;
import demin0u.com.rexit_test_app.fragments.GalleryDialog;
import demin0u.com.rexit_test_app.fragments.PhotosFragment;
import demin0u.com.rexit_test_app.fragments.ProfileFragment;
import demin0u.com.rexit_test_app.R;
import demin0u.com.rexit_test_app.loaders.PhotosLoader;
import demin0u.com.rexit_test_app.loaders.UploadPhotoLoader;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback,
        PhotosFragment.OnFragmentInteractionListener,
        LoaderManager.LoaderCallbacks<BaseResponse> {

    private static final String NAV_ITEM_ID = "navItemId";

    private static final int UPLOAD_PHOTO_LOADER_ID = 1;
    private static final int PHOTOS_LOADER_ID = 2;

    private static final String PHOTO_KEY = "photo";
    private static final String SERVER_TOKEN_KEY = "token";
    private static final String DIALOG_KEY = "dialog";

    private static final int REQUEST_ACCESS_LOCATION = 18;
    private static final int REQUEST_WRITE_FILE = 34;
    private static final int CAPTURE_IMAGE_REQUEST = 14;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private SupportMapFragment mapFragment;

    private int mNavItemId;
    private Uri mImageUri;
    private ArrayList<UserPhoto> photos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tokenTracker.startTracking();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (null == savedInstanceState) {
            mNavItemId = R.id.drawer_item_1;
            requestUserData();
            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getView().setVisibility(View.VISIBLE);
            if (mayRequestLocationPermissions())
                mapFragment.getMapAsync(this);
        } else {
            mNavItemId = savedInstanceState.getInt(NAV_ITEM_ID);
        }

        // listen for navigation events
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().findItem(mNavItemId).setChecked(true);

        // set up the hamburger icon to open and close the drawer
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        navigate(mNavItemId);
    }

    /**
     * Performs the actual navigation logic, updating the main content fragment.
     */
    private void navigate(final int itemId) {
        switch (itemId) {
            case R.id.drawer_item_1:
                requestUserData();
                break;
            case R.id.drawer_item_2:
                mapFragment.getView().setVisibility(View.GONE);
                requestUserFriends();
                break;
            case R.id.drawer_item_3:
                if (mayRequestCameraPermissions())
                    startCamera();
                break;
            case R.id.drawer_item_4:
                mapFragment.getView().setVisibility(View.GONE);
                getUserPhotos();
                break;
            default:
                // ignore
                break;
        }
    }

    /**
     * Handles clicks on the navigation menu.
     */
    @Override
    public boolean onNavigationItemSelected(final MenuItem menuItem) {
        // update highlighted item in the navigation menu
        menuItem.setChecked(true);
        if (menuItem.getItemId() != R.id.drawer_item_3)
            mNavItemId = menuItem.getItemId();

        // allow some time after closing the drawer before performing real navigation
        // so the user can see what is happening
        mDrawerLayout.closeDrawer(GravityCompat.START);
        navigate(menuItem.getItemId());
        return true;
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.support.v7.appcompat.R.id.home) {
            return mDrawerToggle.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(NAV_ITEM_ID, mNavItemId);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(MainActivity.this, R.string.location_permissions_denied, Toast.LENGTH_SHORT).show();
            return;
        }
        googleMap.setMyLocationEnabled(true);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean mayRequestLocationPermissions() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            Log.w("Permissions", "Its not Android M");
            return true;
        }
        if (checkSelfPermission(ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.w("Permissions", "Permissions granted");
            return true;
        } else {
            Log.w("Permissions", "Request permissions");
            requestPermissions(new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION}, REQUEST_ACCESS_LOCATION);
        }
        return false;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean mayRequestCameraPermissions() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            Log.w("Permissions", "Its not Android M");
            return true;
        }
        if (checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Log.w("Permissions", "Permissions granted");
            return true;
        } else {
            Log.w("Permissions", "Request permissions");
            requestPermissions(new String[]{CAMERA, WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_FILE);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_ACCESS_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mapFragment.getMapAsync(this);
            }
        } else if (requestCode == REQUEST_WRITE_FILE) {
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                startCamera();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_IMAGE_REQUEST && resultCode == RESULT_OK) {
            uploadUserPhoto(getPic());
        }
    }

    private void startCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo;
        try {
            photo = createTemporaryFile("picture", ".jpg");
            photo.delete();
        } catch (Exception e) {
            return;
        }
        try {
            Log.d("Profile activity", "File created " + photo.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        mImageUri = Uri.fromFile(photo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, CAPTURE_IMAGE_REQUEST);
        }
    }

    private File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    private Bitmap getPic() {
        this.getContentResolver().notifyChange(mImageUri, null);
        ContentResolver cr = this.getContentResolver();
        try {
            return android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);
        } catch (Exception e) {
            return null;
        }
    }

    private void requestUserData() {
        Bundle bundle = new Bundle();
        bundle.putString("fields", "first_name,last_name,email,location");

        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        final String picture = "https://graph.facebook.com/" + accessToken.getUserId() + "/picture?height=600&width=600&return_ssl_resources=1";

        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        JSONObject location = object.optJSONObject("location");
                        String loc = (location != null) ? location.optString("name") : "";

                        fillUserData(object.optString("first_name"),
                                object.optString("last_name"),
                                object.optString("email"),
                                loc,
                                picture);
                    }
                });
        request.setParameters(bundle);
        request.executeAsync();
    }

    private void requestUserFriends() {
        Bundle bundle = new Bundle();
        bundle.putString("fields", "first_name,last_name");

        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        GraphRequest request = GraphRequest.newMyFriendsRequest(accessToken, new GraphRequest.GraphJSONArrayCallback() {
            @Override
            public void onCompleted(JSONArray objects, GraphResponse response) {
                ArrayList<String> friends = new ArrayList<String>();
                for (int i = 0; i < objects.length(); i++) {
                    try {
                        JSONObject obj = objects.getJSONObject(i);
                        if (obj != null) {
                            friends.add(obj.optString("first_name").concat(" ").concat(obj.optString("last_name")));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                fillFriendsList(friends);
            }
        });

        request.setParameters(bundle);
        request.executeAsync();
    }

    private void uploadUserPhoto(Bitmap photo) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PHOTO_KEY, photo);
        bundle.putString(SERVER_TOKEN_KEY, getSharedPreferences(getPackageName(), MODE_PRIVATE).getString(SERVER_TOKEN_KEY, ""));

        if (getSupportLoaderManager().getLoader(UPLOAD_PHOTO_LOADER_ID) == null)
            getSupportLoaderManager().initLoader(UPLOAD_PHOTO_LOADER_ID, bundle, this).forceLoad();
        else
            getSupportLoaderManager().restartLoader(UPLOAD_PHOTO_LOADER_ID, bundle, this).forceLoad();
    }

    private void getUserPhotos() {
        Bundle bundle = new Bundle();
        bundle.putString(SERVER_TOKEN_KEY, getSharedPreferences(getPackageName(), MODE_PRIVATE).getString(SERVER_TOKEN_KEY, ""));

        if (getSupportLoaderManager().getLoader(PHOTOS_LOADER_ID) == null)
            getSupportLoaderManager().initLoader(PHOTOS_LOADER_ID, bundle, this).forceLoad();
        else
            getSupportLoaderManager().restartLoader(PHOTOS_LOADER_ID, bundle, this).forceLoad();
    }

    private void fillUserData(String name, String lastname, String email, String loc, String imgPath) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, ProfileFragment.newInstance(name, lastname, email, loc, imgPath))
                .commit();
    }

    private void fillFriendsList(ArrayList<String> friends) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, FriendsListFragment.newInstance(friends))
                .commit();
    }

    private AccessTokenTracker tokenTracker = new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
            AccessToken.setCurrentAccessToken(currentAccessToken);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tokenTracker.stopTracking();
    }

    @Override
    public void onPhotoClick(int position) {
        GalleryDialog.newInstance(photos, position).show(getSupportFragmentManager(),DIALOG_KEY);
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case UPLOAD_PHOTO_LOADER_ID:
                return new UploadPhotoLoader(getBaseContext(), args.getString(SERVER_TOKEN_KEY), (Bitmap) args.getParcelable(PHOTO_KEY));
            case PHOTOS_LOADER_ID:
                return new PhotosLoader(getBaseContext(), args.getString(SERVER_TOKEN_KEY));
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        if (loader instanceof UploadPhotoLoader) {
            switch (data.getCode()) {
                case BaseResponse.SUCCESS:
                    Toast.makeText(MainActivity.this, data.getSuccessMessage(), Toast.LENGTH_LONG).show();
                    break;
                default:
                    Toast.makeText(MainActivity.this, data.getErrorMessage(), Toast.LENGTH_LONG).show();
            }
        } else if (loader instanceof PhotosLoader) {
            switch (data.getCode()) {
                case BaseResponse.SUCCESS:
                    photos = (ArrayList<UserPhoto>) data.getResponse();
                    Log.w("finish","length "+photos.size());
                    handler.sendEmptyMessage(1);
                    break;
                default:
                    Toast.makeText(MainActivity.this, data.getErrorMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content, PhotosFragment.newInstance(photos))
                            .commit();
                    break;
            }
        }
    };
}