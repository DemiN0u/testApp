package demin0u.com.rexit_test_app.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

import demin0u.com.rexit_test_app.R;
import demin0u.com.rexit_test_app.api.BaseResponse;
import demin0u.com.rexit_test_app.loaders.AuthorizationLoader;
import demin0u.com.rexit_test_app.loaders.RegistrationLoader;

public class LoginActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<BaseResponse> {

    private static final int REGISTRATION_LOADER_ID = 1;
    private static final int AUUTHORIZATION_LOADER_ID = 2;

    private static final String FB_TOKEN_KEY = "fb_token";
    private static final String SERVER_TOKEN_KEY = "token";

    CallbackManager callbackManager;

    private ProgressBar progressBar;
    private LoginButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        loginButton = (LoginButton) findViewById(R.id.login_button);
        progressBar = (ProgressBar) findViewById(R.id.progrees);

        if(AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired()) {
          authorizeUser(AccessToken.getCurrentAccessToken());
        } else {
            callbackManager = CallbackManager.Factory.create();
            loginButton.setReadPermissions(Arrays.asList("email, user_friends, user_location"));
            loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                   registerUser(loginResult.getAccessToken());
                }

                @Override
                public void onCancel() {
                    Toast.makeText(LoginActivity.this, R.string.login_cancelled, Toast.LENGTH_LONG).show();
                }

                @Override
                public void onError(FacebookException error) {
                    Toast.makeText(LoginActivity.this, error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void registerUser(AccessToken accessToken) {
        Bundle bundle = new Bundle();
        bundle.putString(FB_TOKEN_KEY, accessToken.getToken());
        if (getSupportLoaderManager().getLoader(REGISTRATION_LOADER_ID) == null)
            getSupportLoaderManager().initLoader(REGISTRATION_LOADER_ID, bundle, this).forceLoad();
        else
            getSupportLoaderManager().restartLoader(REGISTRATION_LOADER_ID, bundle, this).forceLoad();
    }

    private void authorizeUser(AccessToken accessToken) {
        Bundle bundle = new Bundle();
        bundle.putString(FB_TOKEN_KEY, accessToken.getToken());
        if (getSupportLoaderManager().getLoader(AUUTHORIZATION_LOADER_ID) == null)
            getSupportLoaderManager().initLoader(AUUTHORIZATION_LOADER_ID, bundle, this).forceLoad();
        else
            getSupportLoaderManager().restartLoader(AUUTHORIZATION_LOADER_ID, bundle, this).forceLoad();
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        showProgress(true);
        switch(id) {
            case REGISTRATION_LOADER_ID:
                return new RegistrationLoader(getBaseContext(),args.getString(FB_TOKEN_KEY));
            case AUUTHORIZATION_LOADER_ID:
                return new AuthorizationLoader(getBaseContext(),args.getString(FB_TOKEN_KEY));
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        showProgress(false);

        if(loader instanceof RegistrationLoader) {
            switch (data.getCode()) {
                case BaseResponse.BAD_REQUEST:
                case BaseResponse.SUCCESS:
                    authorizeUser(AccessToken.getCurrentAccessToken());
                    break;
                default:
                    Toast.makeText(LoginActivity.this, data.getErrorMessage(), Toast.LENGTH_LONG).show();
            }
        } else if(loader instanceof AuthorizationLoader) {
            switch (data.getCode()) {
                case BaseResponse.SUCCESS:
                    SharedPreferences sp = getSharedPreferences(getPackageName(),MODE_PRIVATE);
                    sp.edit().putString(SERVER_TOKEN_KEY, String.valueOf(data.getResponse())).apply();
                    startMainActivity();
                    break;
                default:
                    Toast.makeText(LoginActivity.this, data.getErrorMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {}

    private void showProgress(boolean show) {
        if(show) {
            loginButton.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            loginButton.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }
}
