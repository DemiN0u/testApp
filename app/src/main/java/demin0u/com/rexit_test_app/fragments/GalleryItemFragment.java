package demin0u.com.rexit_test_app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import demin0u.com.rexit_test_app.R;

public class GalleryItemFragment extends Fragment {

    private static final String IMAGE_KEY = "image";

    private ImageView imageView;

    private View view;
    private String image;

    public GalleryItemFragment() {}

    public static GalleryItemFragment newInstance(String image) {
        GalleryItemFragment fragment = new GalleryItemFragment();
        Bundle args = new Bundle();
        args.putString(IMAGE_KEY, image);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            image = getArguments().getString(IMAGE_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_gallery_item, container, false);

        imageView = (ImageView) view.findViewById(R.id.image);

        Picasso.with(getActivity())
                .load(image)
                .memoryPolicy(MemoryPolicy.NO_STORE)
                .resize(getActivity().getResources().getDimensionPixelSize(R.dimen.image_width),
                        getActivity().getResources().getDimensionPixelSize(R.dimen.image_height))
                .into(imageView);

        return view;
    }
}
