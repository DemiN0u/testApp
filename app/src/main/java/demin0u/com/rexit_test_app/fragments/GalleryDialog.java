package demin0u.com.rexit_test_app.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import demin0u.com.rexit_test_app.R;
import demin0u.com.rexit_test_app.adapters.GalleryAdapter;
import demin0u.com.rexit_test_app.data.UserPhoto;

/**
 * Created by DemiN0u on 04.03.2016.
 */
public class GalleryDialog extends DialogFragment {

    private static final String IMAGES_KEY = "images";
    private static final String POSITION_KEY = "position";

    private ViewPager pager;

    private int position;
    private ArrayList<UserPhoto> items;

    public static GalleryDialog newInstance(ArrayList<UserPhoto> images, int startPosition) {
        GalleryDialog fragment = new GalleryDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(IMAGES_KEY, images);
        bundle.putInt(POSITION_KEY, startPosition);
        fragment.setArguments(bundle);
        fragment.setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Translucent_NoTitleBar);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            items = getArguments().getParcelableArrayList(IMAGES_KEY);
            position = getArguments().getInt(POSITION_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);

        pager = (ViewPager) view.findViewById(R.id.gallery_pager);

        pager.setAdapter(new GalleryAdapter(getChildFragmentManager(), items));
        pager.setOffscreenPageLimit(1);
        pager.setCurrentItem(position);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        pager.removeAllViews();

    }
}
