package demin0u.com.rexit_test_app.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import demin0u.com.rexit_test_app.R;
import demin0u.com.rexit_test_app.adapters.PhotosAdapter;
import demin0u.com.rexit_test_app.data.UserPhoto;

public class PhotosFragment extends Fragment {

    private static final String PHOTOS_KEY = "photos";


    private ArrayList<UserPhoto> photos;

    private OnFragmentInteractionListener listener;

    public PhotosFragment() {
        // Required empty public constructor
    }

    public static PhotosFragment newInstance(ArrayList<UserPhoto> photos) {
        PhotosFragment fragment = new PhotosFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(PHOTOS_KEY, photos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            photos = getArguments().getParcelableArrayList(PHOTOS_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photos, container, false);

        GridView gridview = (GridView) view.findViewById(R.id.gridview);
        PhotosAdapter adapter = new PhotosAdapter(getActivity(),photos);
        gridview.setAdapter(adapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                listener.onPhotoClick(position);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface OnFragmentInteractionListener {
        void onPhotoClick(int position);
    }
}
