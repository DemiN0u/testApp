package demin0u.com.rexit_test_app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import demin0u.com.rexit_test_app.R;

public class ProfileFragment extends Fragment {

    private static final String NAME_KEY = "name";
    private static final String LASTNAME_KEY = "lastname";
    private static final String EMAIL_KEY = "email";
    private static final String LOC_KEY = "loc";
    private static final String IMG_KEY = "img";

    private String name;
    private String lastname;
    private String email;
    private String loc;
    private String imgPath;

    public ProfileFragment() {}

    public static ProfileFragment newInstance(String... fields) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NAME_KEY,fields[0]);
        bundle.putString(LASTNAME_KEY,fields[1]);
        bundle.putString(EMAIL_KEY,fields[2]);
        bundle.putString(LOC_KEY,fields[3]);
        bundle.putString(IMG_KEY,fields[4]);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
           name     = getArguments().getString(NAME_KEY);
           lastname = getArguments().getString(LASTNAME_KEY);
           email    = getArguments().getString(EMAIL_KEY);
           loc    = getArguments().getString(LOC_KEY);
           imgPath  = getArguments().getString(IMG_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        ImageView image = (ImageView) view.findViewById(R.id.profile_image);
        TextView nameView = (TextView) view.findViewById(R.id.profile_name);
        TextView emailView = (TextView) view.findViewById(R.id.profile_email);
        TextView locationView = (TextView) view.findViewById(R.id.location);

        nameView.setText(name.concat(" ").concat(lastname));
        emailView.setText(email);
        locationView.setText(loc);

        Picasso.with(getActivity()).load(imgPath).placeholder(R.mipmap.ic_launcher).into(image);

        return view;
    }
}
