package demin0u.com.rexit_test_app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import demin0u.com.rexit_test_app.adapters.FriendsListAdapter;
import demin0u.com.rexit_test_app.R;

public class FriendsListFragment extends Fragment {

    private static final String LIST_KEY = "items";

    private ArrayList<String> items;

    public FriendsListFragment() {}

    public static FriendsListFragment newInstance(ArrayList<String> items) {
        FriendsListFragment fragment = new FriendsListFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(LIST_KEY,items);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            items = getArguments().getStringArrayList(LIST_KEY);
        }

        if(items == null) items = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(new FriendsListAdapter(items));
        }
        return view;
    }
}
