package demin0u.com.rexit_test_app.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import demin0u.com.rexit_test_app.api.BaseResponse;
import demin0u.com.rexit_test_app.api.Requests;

/**
 * Created by DemiN0u on 27.07.2016.
 */
public class RegistrationLoader extends AsyncTaskLoader<BaseResponse> {

    private String fbToken;

    public RegistrationLoader(Context context, String fbToken) {
        super(context);
        this.fbToken = fbToken;
    }

    @Override
    public BaseResponse loadInBackground() {
        Requests requests = new Requests();
        return requests.register(fbToken);
    }
}
