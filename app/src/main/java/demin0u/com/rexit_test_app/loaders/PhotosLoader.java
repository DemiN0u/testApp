package demin0u.com.rexit_test_app.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import demin0u.com.rexit_test_app.api.BaseResponse;
import demin0u.com.rexit_test_app.api.Requests;

/**
 * Created by DemiN0u on 27.07.2016.
 */
public class PhotosLoader extends AsyncTaskLoader<BaseResponse> {

    private String token;

    public PhotosLoader(Context context, String token) {
        super(context);
        this.token = token;
    }

    @Override
    public BaseResponse loadInBackground() {
        Requests requests = new Requests(token);
        return requests.getPhotos();
    }
}
