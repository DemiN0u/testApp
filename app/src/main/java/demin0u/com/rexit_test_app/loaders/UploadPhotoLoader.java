package demin0u.com.rexit_test_app.loaders;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import demin0u.com.rexit_test_app.api.BaseResponse;
import demin0u.com.rexit_test_app.api.Requests;

/**
 * Created by DemiN0u on 01.03.2016.
 */
public class UploadPhotoLoader extends AsyncTaskLoader<BaseResponse> {

    private String token;
    private Bitmap image;
    private File file;

    public UploadPhotoLoader(Context context, String token, Bitmap bitmap) {
        super(context);
        this.token = token;
        this.image = bitmap;
        file = new File(context.getCacheDir(), "user_img");

    }

    @Override
    public BaseResponse loadInBackground() {
        Requests requests = new Requests(token);
        return requests.uploadImage(convertImageToFile());
    }

    private File convertImageToFile(){
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            FileOutputStream fos = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.JPEG, 0, bos);
            byte[] bitmapdata = bos.toByteArray();
            fos.write(bitmapdata);
            Log.w("upload image", "convert success:" + file.length());
            bos.close();
            fos.close();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            Log.w("upload image", "error:" + e.getMessage());
            return null;
        }
    }
}
