package demin0u.com.rexit_test_app.api;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by DemiN0u on 29.02.2016.
 */
public class HeaderInterceptor implements Interceptor {

    private String header;

    public HeaderInterceptor(String value) {
        this.header = value;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer ".concat(header)).build();

        return chain.proceed(request);
    }
}
