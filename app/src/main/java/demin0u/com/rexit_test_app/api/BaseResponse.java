package demin0u.com.rexit_test_app.api;

/**
 * Created by DemiN0u on 27.07.2016.
 */
public class BaseResponse<T> {

    public final static int PARSE_ERROR = 6;

    public final static int BAD_REQUEST = 400;
    public final static int REQUEST_TIMEOUT = 408;
    public final static int TOO_LARGE = 413;
    public final static int SERVER_ERROR = 500;
    public final static int BAD_GATEWAY = 502;
    public final static int NOT_RESPONSE = 503;
    public final static int TIMEOUT = 504;
    public final static int SUCCESS = 200;

    private String successMessage;

    private String errorMessage;

    private int code;

    private T response;

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }
}
