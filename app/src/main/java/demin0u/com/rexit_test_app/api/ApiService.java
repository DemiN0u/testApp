package demin0u.com.rexit_test_app.api;

import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import java.util.List;
import java.util.Map;

import demin0u.com.rexit_test_app.data.UserPhoto;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.GET;

/**
 * Created by DemiN0u on 02.12.2015.
 */
public interface ApiService {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("/user")
    Call<ResponseBody> register(@Body Map map);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("/login")
    Call<ResponseBody> login(@Body Map map);

    @Headers({
            "Content-Type: multipart/form-data, boundary=----WebKitFormBoundaryB8T6D4EdaLGv3WTl"
    })
    @Multipart
    @POST("/user/photo")
    Call<ResponseBody> uploadImage(@Part("photo\"; filename=\"QSUlrrlm.png") RequestBody file);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("/user/photo")
    Call<List<UserPhoto>> getPhotos();
}
