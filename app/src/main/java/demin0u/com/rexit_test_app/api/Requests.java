package demin0u.com.rexit_test_app.api;

import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import demin0u.com.rexit_test_app.data.UserPhoto;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by DemiN0u on 04.12.2015.
 */
public class Requests {

    private static final String BASE_URL = "http://smachnogo.rexit.info:8088";

    private Retrofit retrofit;
    private ApiService service;

    public Requests() {
        retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(ApiService.class);
    }

    public Requests(String header) {
        retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(addTokenHeader(header))
                .build();
        service = retrofit.create(ApiService.class);
    }

    private OkHttpClient addTokenHeader(String value) {
        OkHttpClient httpClient = new OkHttpClient();
        httpClient.networkInterceptors().add(new HeaderInterceptor(value));
        return httpClient;
    }

    public BaseResponse<String> register(String fbToken) {
        BaseResponse<String> res = new BaseResponse<>();
        Map<String, String> map = new HashMap<>();
        map.put("facebook_token", fbToken);
        Call<ResponseBody> call = service.register(map);

        try {
            Response response = call.execute();
            if (response.isSuccess() && response.body() != null && response.code() == 200) {
                ResponseBody body = (ResponseBody) response.body();
                JSONObject obj = new JSONObject(body.string());
                res.setResponse(obj.optString("token"));
                res.setCode(response.code());
                res.setSuccessMessage(response.message());
                res.setErrorMessage(null);
                return res;
            } else {
                res.setResponse(null);
                res.setSuccessMessage(null);
                res.setCode(response.code());
                res.setErrorMessage(response.message());
                return res;
            }
        } catch (IOException e) {
            e.printStackTrace();
            res.setResponse(null);
            res.setSuccessMessage(null);
            res.setErrorMessage(e.getMessage());
            res.setCode(BaseResponse.PARSE_ERROR);
            return res;
        } catch (JSONException e) {
            e.printStackTrace();
            res.setResponse(null);
            res.setSuccessMessage(null);
            res.setErrorMessage(e.getMessage());
            res.setCode(BaseResponse.PARSE_ERROR);
            return res;
        }
    }

    public BaseResponse<String> login(String fbToken) {
        BaseResponse<String> res = new BaseResponse<>();
        Map<String, String> map = new HashMap<>();
        map.put("facebook_token", fbToken);
        Call<ResponseBody> call = service.login(map);

        try {
            Response response = call.execute();
            if (response.isSuccess() && response.body() != null && response.code() == 200) {
                ResponseBody body = (ResponseBody) response.body();
                JSONObject obj = new JSONObject(body.string());
                res.setResponse(obj.optString("token"));
                res.setCode(response.code());
                res.setSuccessMessage(response.message());
                res.setErrorMessage(null);
                return res;
            } else {
                res.setResponse(null);
                res.setSuccessMessage(null);
                res.setCode(response.code());
                res.setErrorMessage(response.message());
                return res;
            }
        } catch (IOException e) {
            e.printStackTrace();
            res.setResponse(null);
            res.setSuccessMessage(e.getMessage());
            res.setErrorMessage(null);
            res.setCode(BaseResponse.PARSE_ERROR);
            return res;
        } catch (JSONException e) {
            e.printStackTrace();
            res.setResponse(null);
            res.setSuccessMessage(null);
            res.setErrorMessage(e.getMessage());
            res.setCode(BaseResponse.PARSE_ERROR);
            return res;
        }
    }

    public BaseResponse<Boolean> uploadImage(File file) {
        BaseResponse<Boolean> res = new BaseResponse<>();
        RequestBody requestBody =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);
        Call<ResponseBody> call = service.uploadImage(requestBody);

        try {
            Response response = call.execute();
            if (response.isSuccess() && response.code() == 200) {
                Log.d("Upload photo", response.message());
                res.setResponse(response.isSuccess());
                res.setCode(response.code());
                res.setSuccessMessage(response.message());
                res.setErrorMessage(null);
                return res;
            } else {
                Log.d("Upload photo", response.message());
                res.setResponse(null);
                res.setSuccessMessage(null);
                res.setCode(response.code());
                res.setErrorMessage(response.message());
                return res;
            }
        } catch (IOException e) {
            e.printStackTrace();
            res.setResponse(null);
            res.setSuccessMessage(null);
            res.setErrorMessage(e.getMessage());
            res.setCode(BaseResponse.PARSE_ERROR);
            return res;
        }
    }

    public BaseResponse<List<UserPhoto>> getPhotos() {
        BaseResponse<List<UserPhoto>> res = new BaseResponse<>();
        List<UserPhoto> photos = new ArrayList<>();

        Call<List<UserPhoto>> call = service.getPhotos();

        try {
            Response<List<UserPhoto>> response = call.execute();
            Log.w("Location responce", "response " + response.message());
            if (response.isSuccess() && response.body() != null && response.code() == 200) {
                res.setResponse(response.body());
                res.setCode(response.code());
                res.setSuccessMessage(response.message());
                res.setErrorMessage(null);
                return res;
            } else {
                res.setResponse(photos);
                res.setSuccessMessage(null);
                res.setCode(response.code());
                res.setErrorMessage(response.message());
                return res;
            }
        } catch (IOException e) {
            e.printStackTrace();
            res.setResponse(photos);
            res.setSuccessMessage(null);
            res.setErrorMessage(e.getMessage());
            res.setCode(BaseResponse.PARSE_ERROR);
            return res;
        }
    }
}
