package demin0u.com.rexit_test_app.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import demin0u.com.rexit_test_app.R;
import demin0u.com.rexit_test_app.data.UserPhoto;

/**
 * Created by DemiN0u on 26.07.2016.
 */
public class PhotosAdapter extends BaseAdapter {

    private Context mContext;
    private List<UserPhoto> items;

    public PhotosAdapter(Context c, List<UserPhoto> photos) {
        mContext = c;
        this.items = photos;
    }

    public int getCount() {
        return items.size();
    }

    public Object getItem(int position) {
        return items.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(
                    new GridView.LayoutParams(mContext.getResources().getDimensionPixelSize(R.dimen.thumb_width),
                            mContext.getResources().getDimensionPixelSize(R.dimen.thumb_height)));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }

        Picasso.with(mContext)
                .load(items.get(position).getUrl())
                .resize(mContext.getResources().getDimensionPixelSize(R.dimen.thumb_width),
                        mContext.getResources().getDimensionPixelSize(R.dimen.thumb_height))
                .memoryPolicy(MemoryPolicy.NO_STORE)
                .into(imageView);

        return imageView;
    }
}