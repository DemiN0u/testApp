package demin0u.com.rexit_test_app.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import demin0u.com.rexit_test_app.data.UserPhoto;
import demin0u.com.rexit_test_app.fragments.GalleryItemFragment;

/**
 * Created by DemiN0u on 04.03.2016.
 */
public class GalleryAdapter extends FragmentPagerAdapter {

    private List<UserPhoto> items;

    public GalleryAdapter(FragmentManager fm, List<UserPhoto> items) {
        super(fm);
        this.items = items;
    }

    @Override
    public Fragment getItem(int position) {
        return GalleryItemFragment.newInstance(items.get(position).getUrl());
    }

    @Override
    public int getCount() {
        return items.size();
    }
}
